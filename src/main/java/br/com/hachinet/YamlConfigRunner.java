package br.com.hachinet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.yaml.snakeyaml.Yaml;

@SuppressWarnings("unchecked")
public class YamlConfigRunner {

  public static void main(String[] args) throws IOException {

    File f = null;
    if (args.length == 1) {
      f = new File(args[0]);
    }
    if (f == null || !f.exists() || f.isDirectory()) {
      System.out.println("yaml file invalid");
      System.exit(1);
    }

    Yaml yaml = new Yaml();

    InputStream in = new FileInputStream(f);
    TreeMap<String, Map<String, Object>> config = yaml.loadAs(in, TreeMap.class);
    System.out.println(String.format("%s%n\nconverts to properties:%n%n%s", config.toString(), toProperties(config)));

    List<String> result = toPropertiesList(config);
    String fileName = FilenameUtils.getBaseName(f.getPath());
    String filePath = FilenameUtils.getFullPath(f.getPath());
    File fileToGen = new File(filePath + fileName + ".properties");
    if (fileToGen.getParentFile() != null) {
      fileToGen.getParentFile().mkdirs();
    }
    FileUtils.writeLines(fileToGen, result);
  }

  private static List<String> toPropertiesList(TreeMap<String, Map<String, Object>> config) {
    List<String> resultList = new ArrayList<String>();
    for (String key : config.keySet()) {
      resultList.addAll(toList(key, config.get(key)));
    }
    return resultList;
  }

  private static List<String> toList(String key, Map<String, Object> map) {
    List<String> resultList = new ArrayList<String>();
    for (String mapKey : map.keySet()) {
      if (map.get(mapKey) instanceof Map) {
        resultList.addAll(toList(String.format("%s.%s", key, mapKey), (Map<String, Object>) map.get(mapKey)));
      } else {
        if (map.get(mapKey) == null)
          resultList.add(String.format("%s.%s=", key, mapKey));
        else
          resultList.add(String.format("%s.%s=%s", key, mapKey, map.get(mapKey).toString()));
      }
    }
    return resultList;
  }

  private static String toProperties(TreeMap<String, Map<String, Object>> config) {
    StringBuilder sb = new StringBuilder();
    for (String key : config.keySet()) {
      String temp = toString(key, config.get(key));
      sb.append(temp);
    }
    return sb.toString();
  }

  private static String toString(String key, Map<String, Object> map) {
    StringBuilder sb = new StringBuilder();
    for (String mapKey : map.keySet()) {
      if (map.get(mapKey) instanceof Map) {
        sb.append(toString(String.format("%s.%s", key, mapKey), (Map<String, Object>) map.get(mapKey)));
      } else {
        if (map.get(mapKey) == null)
          sb.append(String.format("%s.%s=", key, mapKey));
        else
          sb.append(String.format("%s.%s=%s%n", key, mapKey, map.get(mapKey).toString()));
      }
    }
    return sb.toString();
  }

}
